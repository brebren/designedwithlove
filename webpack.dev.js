const merge = require('webpack-merge')
const common = require('./webpack.common')

const config = merge(common, {
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './public',
    },
    output: {
        publicPath: '/js'
    }
})

module.exports = config
