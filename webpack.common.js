const path = require('path');

const config = {
    entry: './src/app/app.js',
    output: {
        libraryTarget: "var",
        library: "App",
        filename: 'app.js',
        path: path.resolve(__dirname, 'public/js')
    }
}

module.exports = config
