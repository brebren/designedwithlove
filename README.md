# Prerequisites
[Yarn](https://yarnpkg.com/en/)

# How to run
In terminal, run one of these

## Dev mode, starts dev-server with hot reload
yarn start

## Build prod mode (for testing), minifies main app.js payload
yarn build

## Release to firebase and designedwith.love
yarn release
