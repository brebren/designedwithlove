import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/storage'
import '@firebase/firestore'
import './header'
import './footer'

// firestore
let db = null;

const storeUser = (user) => {
    db.doc("users/" + user.uid).set({
        displayName: user.displayName,
        email: user.email,
        emailVerified: user.emailVerified,
        photoUrl: user.photoURL,
    }).then(() => {
        console.log("Document written.");
    }).catch((error) => {
        console.error("Error adding document: ", error);
    })
}

const createProject = (data) => {
    const user = firebase.auth().currentUser
    if (user === null) {
        console.log('not logged in')
        return
    }

    const admins = {}
    admins[user.uid] = true

    data.active = true
    data.admins = Object.assign({}, data.admins, admins)
    data.created = firebase.firestore.FieldValue.serverTimestamp()

    return db.collection('projects')
        .add(data)
        .then(() => {
            console.log("Project created.");
        }).catch((error) => {
            console.error("Error creating project: ", error);
        })
}

const updateProject = (id, data) => {
    db.doc(`projects/${id}`).add({})
}

const getProjectsReference = () => {
    return db.collection('projects')
}

const init = () => {
    const config = {
        apiKey: "AIzaSyADmHGONCGgCCDyuop1Co1t8UhNcvHAaZA",
        authDomain: "designed-with-love.firebaseapp.com",
        databaseURL: "https://designed-with-love.firebaseio.com",
        projectId: "designed-with-love",
        storageBucket: "designed-with-love.appspot.com",
        messagingSenderId: "155208065310"
    };

    firebase.initializeApp(config);
    db = firebase.firestore()

    console.log('firebase initialized')
}

const googleLogin = () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider);
}
const signOut = () => firebase.auth().signOut()
const onAuthStateChanged = (callback) => {
    firebase.auth().onAuthStateChanged(callback)
}

export {
    createProject,
    getProjectsReference,
    googleLogin,
    init,
    onAuthStateChanged,
    signOut
}
