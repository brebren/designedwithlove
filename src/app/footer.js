const createFooter = () => {
    return `<hr>
<div class="footer text-center">
    <a href="http://www.pinkhoof.com/" target="_blank">
        <p>Designed with <img src="img/icons/heart.svg" width="12"> by pinkhoof</p>
    </a>
</div>`
}

const $body = document.querySelector('body')
const $footer = createFooter()
$body.insertAdjacentHTML('beforeend', $footer)
