const createHeaderNavBar = () => {
    return `<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" id="header-nav-bar">
    <div class="container-fluid">
        <a class="navbar-brand" href="main.html"><img src="img/logo.svg" class="img-sm"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="navbarSupportedContent" class="navbar-collapse collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="about.html" id="about">Why design with &#9825; ?</a>
                </li>
                <li class="menu-divider">
                    |
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="welcome.html" id="welcome">Welcome!</a>

                    <div id="logged-in" class="hidden">
                        <div class="inline">
                            <img id="user-photo" src="#" class="hidden img-round img-sm ml-2">
                            <div class="dropdown mt-2 show">
                                <a class="dropdown-toggle" role="button" id="dropdownMenuLoggedIn" href="#"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                </a>
                                <div class="dropdown-menu edge-menu" aria-labelledby="dropdownMenuLoggedIn">
                                    <div class="logged-in-message dropdown-item"></div>
                                    <div class="dropdown-divider"></div>
                                    <a href="#" class="dropdown-item" id="logout">Log out</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>`
}

const $body = document.querySelector('body')
const $headerNavBar = createHeaderNavBar()
$body.insertAdjacentHTML('afterbegin', $headerNavBar)

const activePageState = window.location.pathname.replace('/','').replace('.html','')
const activeNavItem = $body.querySelector(`#header-nav-bar #${activePageState}`)
if (activeNavItem !== null) {
    activeNavItem.classList.add('active')
}
